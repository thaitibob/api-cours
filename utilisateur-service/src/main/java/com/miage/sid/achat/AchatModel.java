package com.miage.sid.achat;

import javax.persistence.Entity;
import javax.persistence.Id;

public class AchatModel {

    private String coursid;
    private String utilisateurid;
    private int carte;


    // Getter and Setter
    public String getCoursid() {
        return coursid;
    }

    public void setCoursid(String coursid) {
        this.coursid = coursid;
    }

    public String getUtilisateurid() {
        return utilisateurid;
    }

    public void setUtilisateurid(String utilisateurid) {
        this.utilisateurid = utilisateurid;
    }

    public int getCarte() {
        return carte;
    }

    public void setCarte(int carte) {
        this.carte = carte;
    }
}
