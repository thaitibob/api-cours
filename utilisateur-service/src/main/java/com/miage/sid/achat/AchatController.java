package com.miage.sid.achat;

import java.net.URI;
import java.util.*;
import javax.transaction.Transactional;

import com.miage.sid.client.CoursHystrixClient;
import com.miage.sid.client.CoursModel;
import com.miage.sid.client.EpisodeModel;
import com.miage.sid.utilisateur.User;
import com.miage.sid.utilisateur.UtilisateurRepository;
import com.miage.sid.visionnage.Visionnage;
import com.miage.sid.visionnage.VisionnageModel;
import com.miage.sid.visionnage.VisionnageRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.*;

@RestController
public class AchatController {

    @Autowired
    CoursHystrixClient coursHystrixClient;

    @Autowired
    UtilisateurRepository utilisateurRepo;

    @Autowired
    VisionnageRepository visionnageRepo;


    @GetMapping(value="/utilisateur/{utilisateurid}/achats")
    public ResponseEntity<?> getAllAchats(@PathVariable("utilisateurid") String utilisateurid) {
        Set<String> all = utilisateurRepo.findById(utilisateurid).get().getAchatCours();
        return ResponseEntity.ok(all);
    }

    @GetMapping(value="/utilisateur/{utilisateurid}/achats/cours")
    public ResponseEntity<?> getAllAchatsCours(@PathVariable("utilisateurid") String utilisateurid) {
        Set<String> all = utilisateurRepo.findById(utilisateurid).get().getAchatCours();
        List<CoursModel> cours = new ArrayList<CoursModel>();
        for (String s : all) {
            cours.add(coursHystrixClient.findCoursById(s));
        }
        return ResponseEntity.ok(cours);
    }

    @GetMapping(value="/utilisateur/{utilisateurid}/achat/cours/{coursid}")
    public ResponseEntity<?> getAchat(@PathVariable("utilisateurid") String utilisateurid,
                                      @PathVariable("coursid") String coursid) {
        Optional<User> utilisateur = utilisateurRepo.findById(utilisateurid);
        if (findAchat(utilisateur, coursid)) {
            return ResponseEntity.ok(coursHystrixClient.findCoursById(coursid));
        }
        return ResponseEntity.badRequest().body("Cours payant");
    }

    private boolean findAchat(Optional<User> utilisateur, String coursid) {
        if (utilisateur.isPresent()) {
            Set<String>achats = utilisateur.get().getAchatCours();
            for (String str : achats) {
                if (str.equals(coursid)) {
                    return true;
                }
            }
        }
        return false;
    }

    @GetMapping(value="/utilisateur/{utilisateurid}/achat/cours/{coursid}/episodes/{episodeid}")
    public ResponseEntity<?> getEpisodeByIdutilisateur(@PathVariable("utilisateurid") String utilisateurid,
                                                     @PathVariable("coursid") String coursid,
                                                     @PathVariable("episodeid") String episodeid) {
        Optional<User> utilisateur = utilisateurRepo.findById(utilisateurid);
        if (findAchat(utilisateur, coursid)) {
            EpisodeModel episode = coursHystrixClient.findEpisodeById(episodeid);
            if (!(episode.getNom().equals("Cours service indisponible"))) {
                utilisateur.get().getAchatCours().add(episodeid);
                User utilisateurResult = utilisateurRepo.save(utilisateur.get());
            }
            return ResponseEntity.ok(episode);
        }
        return ResponseEntity.badRequest().body("Cours pas acheté");
    }

    @PostMapping(value="/achat")
    @Transactional
    public ResponseEntity<?> save(@RequestBody AchatModel achatModel) {
        Optional<User> utilisateurModel = utilisateurRepo.findById(achatModel.getUtilisateurid());
        CoursModel coursModel = coursHystrixClient.findCoursById(achatModel.getCoursid());
        if (!(coursModel.getNom().equals("Cours service indisponible"))) {
            if (achatModel.getCarte() % 2 == 0) {
                if (!(findAchat(utilisateurModel, achatModel.getCoursid()))) {
                    utilisateurModel.get().getAchatCours().add(achatModel.getCoursid());

                    List<EpisodeModel> episodes = coursHystrixClient.findEpisodesByCoursid(achatModel.getCoursid());

                    for (EpisodeModel m : episodes) {
                        VisionnageModel v = new VisionnageModel(m.getId(), utilisateurModel.get(), Visionnage.NON_COMMENCE);
                        visionnageRepo.save(v);
                    }

                    User saved = utilisateurRepo.save(utilisateurModel.get());
                    URI location = linkTo(AchatController.class).slash(saved.getId()).toUri();
                    return ResponseEntity.ok(coursModel);
                }
                return ResponseEntity.badRequest().body("Cours deja achete");
            }
            return ResponseEntity.badRequest().body("Carte incorrect");
        }
        return ResponseEntity.ok(coursModel);
    }
}
