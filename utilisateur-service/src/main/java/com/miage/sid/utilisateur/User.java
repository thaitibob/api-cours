package com.miage.sid.utilisateur;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.miage.sid.visionnage.VisionnageModel;
import org.springframework.stereotype.Component;

import javax.persistence.*;
import java.util.List;
import java.util.Set;

@Component
@Entity
@Table(name = "user")
public class User {

    @Id
    private String id;
    private String mail;
    private String nom;
    private String prenom;
    @Enumerated(EnumType.STRING)
    private Statut statut;
    @ElementCollection
    @JsonProperty()
    private Set<String> achatCours;

    @OneToMany(cascade=CascadeType.ALL, orphanRemoval = true, mappedBy = "user", fetch = FetchType.LAZY)
    private List<VisionnageModel> visionnageModelList;

    // Getter and Setter

    public Statut getStatut() {
        return statut;
    }
    public void setStatut(Statut statut) {
        this.statut = statut;
    }
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public Set<String> getAchatCours() {
        return achatCours;
    }
    public void setAchatCours(Set<String> achatCours) {
        this.achatCours = achatCours;
    }

    public List<VisionnageModel> getVisionnageModelList() {
        return visionnageModelList;
    }

    public void setVisionnageModelList(List<VisionnageModel> visionnageModelList) {
        this.visionnageModelList = visionnageModelList;
    }
}
