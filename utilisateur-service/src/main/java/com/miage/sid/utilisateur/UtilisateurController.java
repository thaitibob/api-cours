package com.miage.sid.utilisateur;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.server.ExposesResourceFor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import static java.util.Objects.requireNonNullElse;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.Link;

@RestController
@ExposesResourceFor(User.class)
public class UtilisateurController {

    @Autowired
    private UtilisateurRepository utilisateurRepo;

    @GetMapping(value="/utilisateurs")
    public ResponseEntity<?> getAllUtilisateurs(@RequestParam(value = "statut", required = false) Statut statut ) {
        Iterable<User> all = utilisateurRepo.findAllByStatut(requireNonNullElse(statut, Statut.ACTIF));
        return ResponseEntity.ok(utilisateurToResource(all));
    }

    @GetMapping(value="/utilisateur/{utilisateurId}")
    public ResponseEntity<?> getUtilisateur(@PathVariable("utilisateurId") String id) {
        return Optional.ofNullable(utilisateurRepo.findById(id))
                .filter(Optional::isPresent)
                .map(i -> new ResponseEntity<>(utilisateurToResource(i.get(), true), HttpStatus.OK))
                .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    @PostMapping(value="/utilisateur")
    @Transactional
    public ResponseEntity<?> save(@RequestBody User utilisateurModel) {
        utilisateurModel.setId(UUID.randomUUID().toString());
        utilisateurModel.setStatut(Statut.ACTIF);
        User saved = utilisateurRepo.save(utilisateurModel);
        URI location = linkTo(UtilisateurController.class).slash(saved.getId()).toUri();
        return ResponseEntity.created(location).build();
    }

    @DeleteMapping(value="/utilisateur/{utilisateurId}")
    @Transactional
    public ResponseEntity<?> deleteUtilisateur(@PathVariable("utilisateurId") String utilisateurId) {
        Optional<User> utilisateur = utilisateurRepo.findById(utilisateurId);
        if (utilisateur.isPresent()) {
            utilisateur.get().setStatut(Statut.SUPPRIME);
            User utilisateurModel = utilisateurRepo.save(utilisateur.get());
        }
        return ResponseEntity.noContent().build();
    }

    @PutMapping(value="/utilisateur")
    @Transactional
    public ResponseEntity<?> updateUtilisateur(@RequestBody User utilisateurModel,
                                           @PathVariable("utilisateurId") String utilisateurId) {
        Optional<User> body = Optional.ofNullable(utilisateurModel);
        if (!body.isPresent()) {
            return ResponseEntity.badRequest().build();
        }
        if (!utilisateurRepo.existsById(utilisateurId)) {
            return ResponseEntity.notFound().build();
        }
        utilisateurModel.setId(utilisateurId);
        User result = utilisateurRepo.save(utilisateurModel);
        return ResponseEntity.ok().build();
    }

    private CollectionModel<EntityModel<User>> utilisateurToResource(Iterable<User> utilisateurs) {
        Link selfLink = linkTo(methodOn(UtilisateurController.class).getAllUtilisateurs(Statut.ACTIF)).withSelfRel();
        List<EntityModel<User>> utilisateurResources = new ArrayList();
        utilisateurs.forEach(utilisateurModel -> utilisateurResources.add(utilisateurToResource(utilisateurModel, false)));
        return  CollectionModel.of(utilisateurResources, selfLink);
    }

    private EntityModel<User> utilisateurToResource(User utilisateurModel, Boolean collection) {
        var selfLink = linkTo(UtilisateurController.class).slash(utilisateurModel.getId()).withSelfRel();
        if (Boolean.TRUE.equals(collection)) {
            Link collectionLink = linkTo(methodOn(UtilisateurController.class).getAllUtilisateurs(Statut.ACTIF))
                    .withRel("collection");
            return EntityModel.of(utilisateurModel, selfLink, collectionLink);
        } else {
            return EntityModel.of(utilisateurModel, selfLink);
        }
    }

}
