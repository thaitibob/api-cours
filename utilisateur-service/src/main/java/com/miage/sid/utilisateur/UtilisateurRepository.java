package com.miage.sid.utilisateur;

import org.springframework.data.jpa.repository.JpaRepository;

public interface UtilisateurRepository extends JpaRepository<User, String> {

    Iterable<User> findAllByStatut(Statut statut);
}
