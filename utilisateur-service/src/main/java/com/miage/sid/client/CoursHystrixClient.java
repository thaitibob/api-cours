package com.miage.sid.client;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import java.util.ArrayList;
import java.util.List;

@Component
public class CoursHystrixClient {

    @Autowired
    CoursClient coursClient;

    // cours

    public List<CoursModel> fallbacklistcours() {
        List <CoursModel> cours = new ArrayList<>();
        cours.add(new CoursModel("Cours service indisponible"));
        return cours;
    }

    @HystrixCommand(fallbackMethod = "fallbacklistcours")
    public List<CoursModel> getAllCours() {
        return coursClient.getAllCours();
    }


    public CoursModel fallbackcours(String coursid) {
        return new CoursModel("Cours service indisponible");
    }

    @HystrixCommand(fallbackMethod = "fallbackcours")
    public CoursModel findCoursByIdDispo(String coursid) {
        return coursClient.findCoursById(coursid);
    }

    @HystrixCommand(fallbackMethod = "fallbackcours")
    public CoursModel findCoursById(String coursid) {
        return coursClient.findCoursById(coursid);
    }



    // episodes

    public EpisodeModel fallbackepisode(String episodeid) {
        return new EpisodeModel("Cours service indisponible");
    }

    @HystrixCommand(fallbackMethod = "fallbackepisode")
    public EpisodeModel findEpisodeById(String episodeid) {
        return coursClient.findEpisodeById(episodeid);
    }

    public List<EpisodeModel> fallbacklistepisode(String episodeid) {
        List <EpisodeModel> episodeModels = new ArrayList<>();
        episodeModels.add(new EpisodeModel("Cours service indisponible"));
        return episodeModels;
    }

    @HystrixCommand(fallbackMethod = "fallbacklistepisode")
    public List<EpisodeModel> findEpisodesByCoursid(String coursid) {
        return coursClient.findEpisodesByCoursid(coursid);
    }
}
