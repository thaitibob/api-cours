package com.miage.sid.client;

import javax.persistence.*;


public class CoursModel {

    @Id
    private String id;
    private String nom;
    private String resume;
    private float prix;

    public CoursModel() {
    }

    public CoursModel(String non_disponible) {
        this.nom=non_disponible;
    }

    public CoursModel(CoursModel coursModel) {
        this.id = coursModel.id;
        this.nom = coursModel.nom;
        this.resume = coursModel.resume;
        this.prix = coursModel.prix;
    }


    // Getter and Setter

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getResume() {
        return resume;
    }

    public void setResume(String resume) {
        this.resume = resume;
    }

    public float getPrix() {
        return prix;
    }

    public void setPrix(float prix) {
        this.prix = prix;
    }
}