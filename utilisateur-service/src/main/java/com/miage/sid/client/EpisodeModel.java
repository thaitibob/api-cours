package com.miage.sid.client;

import com.miage.sid.visionnage.VisionnageModel;

import javax.persistence.*;
import java.util.List;

public class EpisodeModel {

    @Id
    private String id;
    private String coursid;
    private String nom;
    private String web;

    public EpisodeModel() {
    }

    public EpisodeModel(String non_disponible) {
        this.nom = non_disponible;
    }

    public EpisodeModel(String nom, String coursid, String web) {
        this.nom = nom;
        this.coursid = coursid;
        this.web = web;
    }


    // Getter and Setter

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCoursid() { return coursid; }

    public void setCoursid(String coursid) { this.coursid = coursid; }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getWeb() {
        return web;
    }

    public void setWeb(String web) {
        this.web = web;
    }

}