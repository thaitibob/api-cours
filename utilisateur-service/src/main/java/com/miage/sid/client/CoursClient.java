package com.miage.sid.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;


@FeignClient(name = "cours-service")
public interface CoursClient {

    //cours

    @GetMapping("/cours/{coursid}/client")
    CoursModel findCoursById(@PathVariable(name = "coursid") String coursid);

    @GetMapping("/cours/client")
    List<CoursModel> getAllCours();



    // episodes
    @GetMapping("/episodes/{episodeid}/client")
    EpisodeModel findEpisodeById(@PathVariable(name = "episodeid") String episodeid);

    @GetMapping("/episodes/cours/{coursid}/client")
    List<EpisodeModel> findEpisodesByCoursid(@PathVariable(name = "coursid") String coursid);
}
