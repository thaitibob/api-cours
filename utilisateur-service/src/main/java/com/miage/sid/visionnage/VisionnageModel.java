package com.miage.sid.visionnage;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.miage.sid.utilisateur.User;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@Entity
public class VisionnageModel {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String dateEvenement;
    @Enumerated(EnumType.STRING)
    private Visionnage vision;
    private String episode;

    @ManyToOne(fetch = FetchType.LAZY)
    private User user;

    public VisionnageModel() {

    }

    public VisionnageModel(String episode, User user, Visionnage v) {

        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss");
        LocalDateTime now = LocalDateTime.now();

        this.dateEvenement = dtf.format(now);
        this.vision = v;
        this.episode = episode;
        this.user = user;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDateEvenement() {
        return dateEvenement;
    }

    public void setDateEvenement(String dateEvenement) {
        this.dateEvenement = dateEvenement;
    }

    public Visionnage getVision() {
        return vision;
    }

    public void setVision(Visionnage vision) {
        this.vision = vision;
    }

    @JsonIgnore
    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getEpisode() {
        return episode;
    }

    public void setEpisode(String episode) {
        this.episode = episode;
    }
}
