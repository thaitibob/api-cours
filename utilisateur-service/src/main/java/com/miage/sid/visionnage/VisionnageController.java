package com.miage.sid.visionnage;

import com.google.common.base.Enums;
import com.miage.sid.achat.AchatController;
import com.miage.sid.client.CoursHystrixClient;
import com.miage.sid.client.CoursModel;
import com.miage.sid.client.EpisodeModel;
import com.miage.sid.utilisateur.Statut;
import com.miage.sid.utilisateur.User;
import com.miage.sid.utilisateur.UtilisateurController;
import com.miage.sid.utilisateur.UtilisateurRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.server.ExposesResourceFor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.transaction.Transactional;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static java.util.Objects.requireNonNullElse;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@RestController
@ExposesResourceFor(Visionnage.class)
public class VisionnageController {

    @Autowired
    private VisionnageRepository visionnageRepo;

    @Autowired
    private UtilisateurRepository utilisateurRepo;

    @Autowired
    CoursHystrixClient coursHystrixClient;

    @GetMapping(value="/visionnages")
    public ResponseEntity<?> getAllVisionnages(@RequestParam(value = "visionnage", required = false) Visionnage visionnage ) {
        Iterable<VisionnageModel> all = visionnageRepo.findAllByVision(requireNonNullElse(visionnage, Visionnage.NON_COMMENCE));
        return ResponseEntity.ok(all);
    }

    @PostMapping(path="/visionnage/{utilisateurid}/{episodeid}/{statut}")
    @Transactional
    public ResponseEntity<?> save(@PathVariable("utilisateurid") String utilisateurid, @PathVariable("episodeid") String episodeid, @PathVariable("statut") String statut) {
        Optional<User> utilisateurModel = utilisateurRepo.findById(utilisateurid);
        EpisodeModel episodeModel = coursHystrixClient.findEpisodeById(episodeid);
        if(utilisateurModel.isPresent()){
            if (statut.equals(Visionnage.EN_COURS.name()) || statut.equals(Visionnage.TERMINE.name())) {
                VisionnageModel v = new VisionnageModel(episodeid, utilisateurModel.get(), Visionnage.valueOf(statut));
                VisionnageModel save = visionnageRepo.save(v);
                return ResponseEntity.ok().build();
            }else {
                return ResponseEntity.badRequest().build();
            }
        }else{
            return ResponseEntity.notFound().build();
        }
    }

}
