package com.miage.sid.visionnage;

import com.miage.sid.utilisateur.Statut;
import com.miage.sid.utilisateur.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface VisionnageRepository extends JpaRepository<VisionnageModel, String> {
    Iterable<VisionnageModel> findAllByVision(Visionnage visionnage);
}
