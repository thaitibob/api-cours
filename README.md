# Projet API - Jonathan VALLE M2 SID

Code pour de réalisation pour le projet API.

Vous trouverez dans ce dépôt :

* Le sujet : M2-SID-API.pdf
* Un jeu de test à importer dans Posteman
* Un docker-compose permettant le déploiement du projet sur votre machine
* Un rapport pdf pour détailler quelques points


### Configuration nécessaire pour exécuter l'application

Voici les commandes pour Linux, mais vous pouvez évidement trouver facilement les équivalents sur internet pour MAC et WINDOWS.

Installation de docker
```commande
sudo apt install docker.io
sudo systemctl start docker
sudo systemctl enable docker
```
Installation de docker-compose
```commande
sudo apt install docker-compose
```

### Déploiement de l'application

Une fois le dossier cloné, se positionner à la racine du projet.

Se rendre dans le dossier cours-service puis lancer :
```commande
mvn install
```

Se rendre dans le dossier gateway-service puis lancer :
```commande
mvn install
```

Se rendre dans le dossier utilisateur-service puis lancer :
```commande
mvn install
```

Pour lancer les services, se replacer à la racine du projet puis utiliser docker-compose (faire attention de bien avoir Docker de lancé et vérifier les ports). Vous n'avez besoin que du docker-compose pour tout déployer. 
```commande
sudo docker-compose up --build -d
```

Si jamais un port est déjà en utilisation, il est possible d'arrêter l'application sur ce port avec les commandes suivantes: 
```commande
sudo lsof -i:PortApplication
kill -15 <PID_Application>
```


### Vérification

Pour vérifier le bon fonctionnement vous pouvez-vous connecter sur consul et vérifier le déploiement de 2 microservices et de l'api gateway
```commande
http://localhost:8500
```
Pour vérifier le bon fonctionnement de la BDD, connectez-vous sur phpmyadmin : (id:root, pwd:admin). Vous devriez accéder à deux BDD : cours, users
```commande
http://localhost:8081
```
Pour tester les services importer la configuration postman et lancer les tests sur le port
```commande
http://localhost:8080/*
```



