package com.miage.sid.episode;

import com.miage.sid.Statut;

import javax.persistence.*;

@Entity
@Table(name = "episode")
public class EpisodeModel {
    
    @Id
    private String id;
    private String coursid;
    private String nom;
    private String web;
    @Enumerated(EnumType.STRING)
    private Statut statut;

    public EpisodeModel(String nom, String coursid, String lien) {
        this.nom = nom;
        this.coursid = coursid;
        this.web = lien;
        this.statut=Statut.ACTIF;
    }
    
    public EpisodeModel() {
    }

    public EpisodeModel(String nom) {
        this.nom=nom;
    }

    // Getter and Setter
    public Statut getStatut() {
        return statut;
    }
    public void setStatut(Statut statut) {
        this.statut = statut;
    }
    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }
    public String getCoursid() { return coursid; }
    public void setCoursid(String coursId) { this.coursid = coursId; }
    public String getNom() {
        return nom;
    }
    public void setNom(String nom) {
        this.nom = nom;
    }
    public String getWeb() {
        return web;
    }
    public void setWeb(String web) {
        this.web = web;
    }

}