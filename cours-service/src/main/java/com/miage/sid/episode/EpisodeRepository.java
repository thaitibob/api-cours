package com.miage.sid.episode;

import com.miage.sid.Statut;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;

public interface EpisodeRepository extends JpaRepository<EpisodeModel, String> {

    List<EpisodeModel> findByCoursidAndStatut(String coursid, Statut statut);

    Optional <EpisodeModel> findByIdAndStatut(String coursid, Statut statut);

    Iterable<EpisodeModel> findAllByStatut(Statut statut);

    @Query(
            value = "SELECT * FROM episode where episode.idcours = ?2 AND episode.id = ?1",
            nativeQuery = true)
    Optional <EpisodeModel> findByIdAdnIdcours(String episodeId, String coursId);
}