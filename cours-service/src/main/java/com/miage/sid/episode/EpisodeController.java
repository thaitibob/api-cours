package com.miage.sid.episode;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import javax.transaction.Transactional;

import com.miage.sid.Statut;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.server.ExposesResourceFor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import static com.miage.sid.Statut.ACTIF;
import static com.miage.sid.Statut.SUPPRIME;
import static java.util.Objects.requireNonNullElse;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.Link;
import org.springframework.web.bind.annotation.*;


@RestController
@ExposesResourceFor(EpisodeModel.class)
public class EpisodeController {

    @Autowired
    private EpisodeRepository episodeRepo;

    @GetMapping(value="/episodes")
    public ResponseEntity<?> getAllEpisodes(@RequestParam(value = "statut", required = false) Statut statut ) {
        Iterable<EpisodeModel> all = episodeRepo.findAllByStatut(requireNonNullElse(statut, ACTIF));
        return ResponseEntity.ok(episodeToResource(all));
    }

    @GetMapping(value="/episodes/cours/{coursid}")
    public ResponseEntity<?> getAllEpisodesByCoursid(@PathVariable("coursid") String coursid) {
        Iterable<EpisodeModel> all = episodeRepo.findByCoursidAndStatut(coursid, ACTIF);
        return ResponseEntity.ok(episodeToResource(all));
    }

    @GetMapping(value="/episodes/{episodeId}")
    public ResponseEntity<?> getEpisode(@PathVariable("episodeId") String id) {
        return Optional.ofNullable(episodeRepo.findById(id))
                .filter(Optional::isPresent)
                .map(i -> new ResponseEntity<>(episodeToResource(i.get(), true), HttpStatus.OK))
                .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    @Transactional
    @PostMapping(value="/episodes/{coursid}")
    public ResponseEntity<?> save(@RequestBody EpisodeModel episodeModel, @PathVariable("coursid") String coursid) {
        episodeModel.setId(UUID.randomUUID().toString());
        episodeModel.setCoursid(coursid);
        EpisodeModel saved = episodeRepo.save(episodeModel);
        URI location = linkTo(EpisodeController.class).slash(saved.getId()).toUri();
        return ResponseEntity.created(location).build();
    }

    @Transactional
    @DeleteMapping(value = "/episodes/{episodeId}")
    public ResponseEntity<?> deleteEpisode(@PathVariable("episodeId") String episodeId) {
        Optional<EpisodeModel> episode = episodeRepo.findById(episodeId);
        if (episode.isPresent()) {
            episode.get().setStatut(SUPPRIME);
            EpisodeModel episodeResult = episodeRepo.save(episode.get());
        }
        return ResponseEntity.noContent().build();
    }

    @Transactional
    @PutMapping(value = "/episode/{episodeId}")
    public ResponseEntity<?> updateEpisode(@RequestBody EpisodeModel episodeModel,
                                         @PathVariable("episodeId") String episodeId) {
        Optional<EpisodeModel> body = Optional.ofNullable(episodeModel);
        if (!body.isPresent()) {
            return ResponseEntity.badRequest().build();
        }
        if (!episodeRepo.existsById(episodeId)) {
            return ResponseEntity.notFound().build();
        }
        episodeModel.setId(episodeId);
        EpisodeModel result = episodeRepo.save(episodeModel);
        return ResponseEntity.ok().build();
    }

    // Utilisateur service

    @GetMapping(value="/episodes/cours/{coursId}/client")
    public List<EpisodeModel> getAllEpisodesByCoursidClient(@PathVariable("coursId") String coursId) {
        return episodeRepo.findByCoursidAndStatut(coursId, ACTIF);
    }

    @GetMapping(value="/episodes/{episodeid}/client")
    public EpisodeModel getEpisodeClient(@PathVariable("episodeid") String episodeid) {
        return Optional.ofNullable(episodeRepo.findByIdAndStatut(episodeid, ACTIF))
                .filter(Optional::isPresent)
                .map(i -> (i.get()))
                .orElse(new EpisodeModel("Not found"));
    }

    // Hateoas

    private CollectionModel<EntityModel<EpisodeModel>> episodeToResource(Iterable<EpisodeModel> episodes) {
        Link selfLink = linkTo(methodOn(EpisodeController.class).getAllEpisodes(ACTIF)).withSelfRel();
        List<EntityModel<EpisodeModel>> episodeResources = new ArrayList();
        episodes.forEach(episodeModel -> episodeResources.add(episodeToResource(episodeModel, false)));
        return  CollectionModel.of(episodeResources, selfLink);
    }

    private EntityModel<EpisodeModel> episodeToResource(EpisodeModel episodeModel, Boolean collection) {
        var selfLink = linkTo(EpisodeController.class).slash(episodeModel.getId()).withSelfRel();
        if (Boolean.TRUE.equals(collection)) {
            Link collectionLink = linkTo(methodOn(EpisodeController.class).getAllEpisodes(ACTIF))
                    .withRel("collection");
            return EntityModel.of(episodeModel, selfLink, collectionLink);
        } else {
            return EntityModel.of(episodeModel, selfLink);
        }
    }
}