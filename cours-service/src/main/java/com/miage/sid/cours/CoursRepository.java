package com.miage.sid.cours;
import com.miage.sid.Statut;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface CoursRepository extends JpaRepository<CoursModel,String> {

    Optional <CoursModel> findByIdAndStatut(String coursid, Statut statut);

    Iterable<CoursModel> findAllByStatut(Statut statut);

}
