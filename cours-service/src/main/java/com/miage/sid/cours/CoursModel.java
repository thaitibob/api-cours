package com.miage.sid.cours;

import com.miage.sid.Statut;

import javax.persistence.*;

@Entity
@Table(name = "cours")
public class CoursModel {

    @Id
    private String id;
    private String nom;
    private String resume;
    private float prix;
    @Enumerated(EnumType.STRING)
    private Statut statut;


    public CoursModel(CoursModel coursModel) {
        this.id = coursModel.id;
        this.nom = coursModel.nom;
        this.resume = coursModel.resume;
        this.prix = coursModel.prix;
        this.statut=Statut.ACTIF;
    }

    public CoursModel() {
    }

    public CoursModel(String nom) {
        this.nom=nom;
    }


    // Getter and Setter
    public Statut getStatut() {
        return statut;
    }
    public void setStatut(Statut statut) {
        this.statut = statut;
    }
    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }
    public String getNom() {
        return nom;
    }
    public void setNom(String nom) {
        this.nom = nom;
    }
    public String getResume() {
        return resume;
    }
    public void setResume(String resume) {
        this.resume = resume;
    }
    public float getPrix() {
        return prix;
    }
    public void setPrix(float prix) {
        this.prix = prix;
    }
}