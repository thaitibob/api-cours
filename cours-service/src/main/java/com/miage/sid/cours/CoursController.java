package com.miage.sid.cours;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import javax.transaction.Transactional;

import com.miage.sid.Statut;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.server.ExposesResourceFor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import static java.util.Objects.requireNonNullElse;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.Link;


@RestController
@ExposesResourceFor(CoursModel.class)
public class CoursController {

    @Autowired
    private CoursRepository coursRepo;

    // GET all
    @GetMapping(value="/cours")
    public ResponseEntity<?> getAllCours(@RequestParam(value = "statut", required = false) Statut statut) {
        Iterable<CoursModel> all = coursRepo.findAllByStatut(requireNonNullElse(statut, Statut.ACTIF));
        return ResponseEntity.ok(coursToResource(all));
    }

    // GET one
    @GetMapping(value="/cours/{coursid}")
    public ResponseEntity<?> getCours(@PathVariable("coursid") String coursid) {
        return Optional.ofNullable(coursRepo.findByIdAndStatut(coursid, Statut.ACTIF))
                .filter(Optional::isPresent)
                .map(i -> new ResponseEntity<>(coursToResource(i.get(), true), HttpStatus.OK))
                .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    // POST
    @PostMapping(value="/cours")
    @Transactional
    public ResponseEntity<?> save(@RequestBody CoursModel coursModel) {
        coursModel.setId(UUID.randomUUID().toString());
        coursModel.setStatut(Statut.ACTIF);
        CoursModel saved = coursRepo.save(coursModel);
        URI location = linkTo(CoursController.class).slash(saved.getId()).toUri();
        return ResponseEntity.created(location).build();
    }

    // DELETE
    @DeleteMapping(value = "/cours/{coursid}")
    @Transactional
    public ResponseEntity<?> deleteCours(@PathVariable("coursid") String coursid) {
        Optional<CoursModel> cours = coursRepo.findByIdAndStatut(coursid, Statut.ACTIF);
        if (cours.isPresent()) {
            cours.get().setStatut(Statut.SUPPRIME);
            CoursModel coursResult = coursRepo.save(cours.get());
        }
        return ResponseEntity.noContent().build();
    }

    // PUT
    @PutMapping(value = "/cours/{coursid}")
    @Transactional
    public ResponseEntity<?> updateCours(@RequestBody CoursModel coursModel,
                                           @PathVariable("coursid") String coursid) {
        Optional<CoursModel> body = Optional.ofNullable(coursModel);
        if (!body.isPresent()) {
            return ResponseEntity.badRequest().build();
        }
        if (!coursRepo.existsById(coursid)) {
            return ResponseEntity.notFound().build();
        }
        coursModel.setId(coursid);
        CoursModel result = coursRepo.save(coursModel);
        return ResponseEntity.ok().build();
    }

    // Utilisateur service

    @GetMapping(value="/cours/client")
    public Iterable<CoursModel> getAllCours2() {
        Iterable <CoursModel> cours = coursRepo.findAllByStatut(Statut.ACTIF);
        return cours;
    }

    @GetMapping(value="/cours/{coursid}/client")
    public Optional<CoursModel> getCoursById(@PathVariable("coursid") String coursid) {
        Optional <CoursModel> cours = coursRepo.findByIdAndStatut(coursid, Statut.ACTIF);
        if (cours.isPresent()) {
            return cours;
        }
        return Optional.of(new CoursModel("Not found"));
    }

    // Hateoas

    private CollectionModel<EntityModel<CoursModel>> coursToResource(Iterable<CoursModel> courss) {
        Link selfLink = linkTo(methodOn(CoursController.class).getAllCours(Statut.ACTIF)).withSelfRel();
        List<EntityModel<CoursModel>> coursResources = new ArrayList();
        courss.forEach(coursModel -> coursResources.add(coursToResource(coursModel, false)));
        return  CollectionModel.of(coursResources, selfLink);
    }

    private EntityModel<CoursModel> coursToResource(CoursModel coursModel, Boolean collection) {
        var selfLink = linkTo(CoursController.class).slash(coursModel.getId()).withSelfRel();
        if (Boolean.TRUE.equals(collection)) {
            Link collectionLink = linkTo(methodOn(CoursController.class).getAllCours(Statut.ACTIF))
                    .withRel("collection");
            return EntityModel.of(coursModel, selfLink, collectionLink);
        } else {
            return EntityModel.of(coursModel, selfLink);
        }
    }

}
